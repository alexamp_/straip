"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

(function () {
  $(document).ready(function () {
    console.log("ready");
    var arrEle = $("#indication-mark-wrap");
    var subMenuContent = $("#submenu-content");
    var subMenuContentItems = $("#submenu-content .items");
    var startHover = $(arrEle).css("left");
    var mobileMenu = $("#mobile-menu");
    var menuItemContent = $(".subnav.prodotti").html();
    subMenuContentItems.html(menuItemContent);
    subMenuContent.hover(function () {
      //subMenuContentItems.css("display", "block");
      subMenuContent.addClass("active");
      arrEle.css("display", "block");
    }, function () {
      subMenuContentItems.css("display", "none");
      subMenuContent.removeClass();
      console.log("svuoto sottomenu");
      arrEle.css("display", "none");
    });
    $(".mainItem").mouseenter(function () {
      var mainItemActive = true;
      console.log("mainItemActive: ", mainItemActive);
      var hasSubMenu = $(this).children(".subnav").length;
      console.log("has child: ", hasSubMenu);

      if (hasSubMenu) {
        var menuId = $(this).attr("item-id");
        console.log("menuId: ", menuId);
        var menuItemContent = $(".subnav." + menuId).html();
        subMenuContentItems.html(menuItemContent);
        subMenuContentItems.css("display", "flex");
        subMenuContent.css("display", "flex");
        subMenuContent.removeClass();
        subMenuContent.addClass(menuId);
        subMenuContent.addClass("active");
        var subItemActive = true;
        console.log("subItemActive: ", subItemActive);
        arrEle.css("display", "block");
        var left = $(this).position().left;
        $(arrEle).css("left", left + 30);
      } else {
        subMenuContent.css("display", "none");
        subMenuContent.removeClass();
      }
    });
    $(".mainItem").mouseleave(function () {
      arrEle.css("display", "none");
      subMenuContent.removeClass("active");
    });
    $(".hamburger").on("click", function () {
      mobileMenu.css("display", "flex");
    });
    $(".close-btn").on("click", function () {
      mobileMenu.css("display", "none");
    }); // globe js
    // Gen random data

    var N = 20;

    var arcsData = _toConsumableArray(Array(N).keys()).map(function () {
      return {
        startLat: (Math.random() - 0.5) * 180,
        startLng: (Math.random() - 0.5) * 360,
        endLat: (Math.random() - 0.5) * 180,
        endLng: (Math.random() - 0.5) * 360,
        color: [['red', 'white', 'blue', 'green'][Math.round(Math.random() * 3)], ['red', 'white', 'blue', 'green'][Math.round(Math.random() * 3)]]
      };
    });

    var world = Globe().globeImageUrl('//unpkg.com/three-globe/example/img/earth-night.jpg').arcsData(arcsData).width(1500).height(900).backgroundColor("#0a2540").arcColor('color').arcDashLength(function () {
      return Math.random();
    }).arcDashGap(function () {
      return Math.random();
    }).arcDashAnimateTime(function () {
      return Math.random() * 4000 + 500;
    })(document.getElementById('globeDiv'));
    world.controls().autoRotate = true;
    world.controls().autoRotateSpeed = -0.8;
    world.scene().scale.set(1.4, 1.4, 1.4);
  });
})();